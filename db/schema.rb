# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170408222912) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "amounts", force: :cascade do |t|
    t.string   "measurement"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "inventory_item_amounts", force: :cascade do |t|
    t.integer  "inventory_item_id"
    t.integer  "amount_id"
    t.integer  "quantity"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["amount_id"], name: "index_inventory_item_amounts_on_amount_id", using: :btree
    t.index ["inventory_item_id"], name: "index_inventory_item_amounts_on_inventory_item_id", using: :btree
  end

  create_table "inventory_items", force: :cascade do |t|
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "product_id"
    t.integer  "packaging_run_id"
    t.date     "date_packaged"
    t.index ["packaging_run_id"], name: "index_inventory_items_on_packaging_run_id", using: :btree
    t.index ["product_id"], name: "index_inventory_items_on_product_id", using: :btree
  end

  create_table "packaging_runs", force: :cascade do |t|
    t.string   "number"
    t.boolean  "on_hold",    default: true, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.string   "sku"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "removal_amounts", force: :cascade do |t|
    t.integer  "removal_id"
    t.integer  "amount_id"
    t.integer  "quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["amount_id"], name: "index_removal_amounts_on_amount_id", using: :btree
    t.index ["removal_id"], name: "index_removal_amounts_on_removal_id", using: :btree
  end

  create_table "removals", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "inventory_item_id"
    t.string   "reason"
    t.text     "description"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["inventory_item_id"], name: "index_removals_on_inventory_item_id", using: :btree
    t.index ["user_id"], name: "index_removals_on_user_id", using: :btree
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.string   "resource_type"
    t.integer  "resource_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
    t.index ["name"], name: "index_roles_on_name", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "access_token"
    t.string   "slack_id"
    t.string   "name"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree
  end

  add_foreign_key "inventory_item_amounts", "amounts"
  add_foreign_key "inventory_item_amounts", "inventory_items"
  add_foreign_key "removal_amounts", "amounts"
  add_foreign_key "removal_amounts", "removals"
  add_foreign_key "removals", "inventory_items"
  add_foreign_key "removals", "users"
end
