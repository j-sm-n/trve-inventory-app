amounts = Amount.create([{ measurement: '1/6 bbl' },
                         { measurement: '1/2 bbl' },
                         { measurement: 'Cases' }])

beers = Product.create([{ name: 'Wanderlust',
                          sku: '1234',
                          description: 'Belgo-American Pale Ale' },
                        { name: 'Scorn',
                          sku: '5678',
                          description: 'Pale Ale' },
                        { name: 'Aura',
                          sku: '10101',
                          description: 'British Mild' }])

amounts.each do |amount|
  puts "#{amount.measurement} added to db"
end

beers.each do |beer|
  puts "#{beer.name} added to db"
end
