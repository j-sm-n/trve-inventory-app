class AddDateToInventoryItem < ActiveRecord::Migration[5.0]
  def change
    add_column :inventory_items, :date_packaged, :date
  end
end
