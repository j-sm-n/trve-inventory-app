class CreateRemovalAmounts < ActiveRecord::Migration[5.0]
  def change
    create_table :removal_amounts do |t|
      t.references :removal, foreign_key: true
      t.references :amount, foreign_key: true
      t.integer :quantity

      t.timestamps
    end
  end
end
