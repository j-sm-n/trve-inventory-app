class CreateRemovals < ActiveRecord::Migration[5.0]
  def change
    create_table :removals do |t|
      t.references :user, foreign_key: true
      t.references :inventory_item, foreign_key: true
      t.string :reason
      t.text :description

      t.timestamps
    end
  end
end
