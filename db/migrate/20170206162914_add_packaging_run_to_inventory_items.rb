class AddPackagingRunToInventoryItems < ActiveRecord::Migration[5.0]
  def change
    add_reference :inventory_items, :packaging_run
  end
end
