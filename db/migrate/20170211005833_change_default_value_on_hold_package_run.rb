class ChangeDefaultValueOnHoldPackageRun < ActiveRecord::Migration[5.0]
  def change
    change_column_null :packaging_runs, :on_hold, false
    change_column_default :packaging_runs, :on_hold, true
  end
end
