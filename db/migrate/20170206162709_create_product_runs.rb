class CreateProductRuns < ActiveRecord::Migration[5.0]
  def change
    create_table :packaging_runs do |t|
      t.string :number
      t.boolean :on_hold

      t.timestamps
    end
  end
end
