class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.integer :role, default: 0
      t.string :access_token
      t.string :slack_id
      t.string :name
      t.string :email
    end
  end
end
