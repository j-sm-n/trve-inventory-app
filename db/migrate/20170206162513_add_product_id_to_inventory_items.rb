class AddProductIdToInventoryItems < ActiveRecord::Migration[5.0]
  def change
    add_reference :inventory_items, :product
  end
end
