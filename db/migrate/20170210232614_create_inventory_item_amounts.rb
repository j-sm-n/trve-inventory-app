class CreateInventoryItemAmounts < ActiveRecord::Migration[5.0]
  def change
    create_table :inventory_item_amounts do |t|
      t.references :inventory_item, foreign_key: true
      t.references :amount, foreign_key: true
      t.integer :quantity

      t.timestamps
    end
  end
end
