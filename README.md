[![build status](https://gitlab.com/j-sm-n/trve-inventory-app/badges/master/build.svg)](https://gitlab.com/j-sm-n/trve-inventory-app/commits/master) [![coverage report](https://gitlab.com/j-sm-n/trve-inventory-app/badges/master/coverage.svg)](https://gitlab.com/j-sm-n/trve-inventory-app/commits/master)

# vár Inventory application
> Track additions to your inventory and record removals for sales or accidents. See snapshots of your production levels and potential sales by month or for the year. Built for small to medium sized craft breweries.

### Configuration
Clone down the project and do the following to get the app up and running locally:
```
bundle
bundle exec figaro install
```
This creates a commented `config/application.yml` file and adds it to your `.gitignore`. Add your own `SLACK_CLIENT_ID` and `SLACK_CLIENT_SECRET` environment variables to the new `application.yml` file that you can obtain for free from [Slack](https://api.slack.com/).

To see more information on the Figaro gem, here's the [original repository](https://github.com/laserlemon/figaro).

### Deployment
To deploy to the current [staging app](https://var-inventory-staging.herokuapp.com/), use the following command:
```
git checkout development
# make sure development is up-to-date
git push staging development:master
```

To deploy to the [production app](https://var-inventory.herokuapp.com/):
```
git checkout master
# make sure development is up-to-date
git push production master
```

### Wireframes
To see wireframes for the main views of this application, visit the following links:
* [Login page](https://wireframe.cc/12Oken)
* [Current Inventory page](https://wireframe.cc/pAzFlQ)
* [Add Inventory page](https://wireframe.cc/pCAs1Q)
* [Record removal page](https://wireframe.cc/9eozEP)
* [Side bar menu](https://wireframe.cc/IUhkQh)
