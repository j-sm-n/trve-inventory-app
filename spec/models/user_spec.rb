require 'rails_helper'

RSpec.describe User, type: :model do
  it { should validate_presence_of :name }
  it { should validate_presence_of :email }

  def slack_user_info
    {
      "access_token": "12345",
      "user": {
        "name": "Jasmin Hudacsek",
        "id": "1",
        "email": "jasmin@example.com"
      }
    }
  end

  it 'creates new user from omniauth information' do
    expect(User.count).to eq(0)

    slack_user = SlackUser.new(slack_user_info.deep_stringify_keys)
    User.from_omniauth(slack_user)

    expect(User.count).to eq(1)
    expect(User.last.name).to eq(slack_user.name)
  end
end
