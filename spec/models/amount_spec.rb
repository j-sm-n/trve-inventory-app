require 'rails_helper'

RSpec.describe Amount, type: :model do
  it { should validate_presence_of :measurement }
  it { should have_many :inventory_item_amounts }
  it { should have_many :inventory_items }
  it { should have_many :removal_amounts }

  scenario 'has all required attributes' do
    amount = create :amount

    expect(Amount.last.measurement).to eq(amount.measurement)
  end
end
