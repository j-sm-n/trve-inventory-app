require 'rails_helper'

RSpec.describe InventoryItem, type: :model do
  it { should validate_presence_of :date_packaged }
  it { should belong_to :product }
  it { should belong_to :packaging_run }
  it { should have_many :amounts }
  it { should have_many :removals }

  scenario 'has all required attributes' do
    product = create :product
    p_run   = create :packaging_run
    item    = create :inventory_item,
              product_id: product.id,
              packaging_run_id: p_run.id,
              date_packaged: '2006-06-06'

    expect(item.product.name).to eq(product.name)
    expect(item.packaging_run.number).to eq (p_run.number)
    expect(item.date_packaged.mday).to eq(6)
    expect(item.packaging_run.on_hold).to be true
  end

  scenario 'can determine if on hold or not' do
    product = create :product
    p_run   = create :packaging_run
    item    = create :inventory_item,
              product_id: product.id,
              packaging_run_id: p_run.id,
              date_packaged: '2006-06-06'

    expect(item.on_hold?).to be true
  end

  scenario 'returns 2-d array of amounts available and removals' do
    user    = create :user
    amount  = create :amount
    product = create :product
    p_run   = create :packaging_run
    item    = create :inventory_item,
              product_id: product.id,
              packaging_run_id: p_run.id,
              date_packaged: '2006-06-06'
    item.inventory_item_amounts.create( amount: amount,
                                        quantity: 12)
    removal = item.removals.create( user: user,
                                    reason: 'Order',
                                    description: '')
    removal.removal_amounts.create( amount: amount,
                                    quantity: 5)
    result = item.available_amounts_and_removals(removal.id)

    expect(result.first.measurement).to eq(amount.measurement)
    expect(result.first.removal).to eq(5)
    expect(result.first.quantity).to eq(12)
  end
end
