require 'rails_helper'

RSpec.describe SlackUser, type: :model do
  def slack_user_info
    {
      "access_token": "12345",
      "user": {
        "name": "Jasmin Hudacsek",
        "id": "1",
        "email": "jasmin@example.com"
      }
    }
  end

  it 'has all attributes of a Slack user' do
    slack_user = SlackUser.new(slack_user_info.deep_stringify_keys)

    expect(slack_user.access_token).to eq("12345")
    expect(slack_user.name).to eq("Jasmin Hudacsek")
    expect(slack_user.slack_id).to eq("1")
    expect(slack_user.email).to eq("jasmin@example.com")
  end
end
