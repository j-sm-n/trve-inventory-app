require 'rails_helper'

RSpec.describe PackagingRun, type: :model do
  it { should validate_presence_of :number }
  it { should validate_uniqueness_of :number }

  it { should have_many :inventory_items }
end
