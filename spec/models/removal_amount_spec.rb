require 'rails_helper'

RSpec.describe RemovalAmount, type: :model do
  it { should validate_presence_of :quantity }
  it { should validate_numericality_of :quantity }
  it { should belong_to :removal }
  it { should belong_to :amount }
end
