require 'rails_helper'

RSpec.describe Removal, type: :model do
  it { should validate_presence_of :reason }
  it { should belong_to :user }
  it { should belong_to :inventory_item }
  it { should have_many :amounts }
end
