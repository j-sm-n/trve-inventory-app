require 'rails_helper'

RSpec.describe Product, type: :model do
  it { should validate_presence_of :name }
  it { should validate_uniqueness_of :name }

  it { should validate_presence_of :sku }
  it { should validate_uniqueness_of :sku }

  it { should validate_presence_of :description }
  
  it { should have_many :inventory_items }
end
