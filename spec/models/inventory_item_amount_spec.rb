require 'rails_helper'

RSpec.describe InventoryItemAmount, type: :model do
  it { should validate_presence_of :quantity }
  it { should validate_numericality_of :quantity }
  it { should belong_to :inventory_item }
  it { should belong_to :amount }
end
