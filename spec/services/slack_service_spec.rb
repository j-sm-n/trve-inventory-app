require 'rails_helper'

RSpec.describe 'Slack service', :type => :service do
  it 'stores the passed Slack code' do
    slack_service = SlackService.new("code")

    expect(slack_service.slack_code).to eq("code")
  end

  def slack_user_info
    { "ok"=>true,
      "access_token"=>"xoxp-133746728865-133746729169-135604655511-4a4b9d359d9d54e964b7ed79c70cb3e9",
      "scope"=>"identity.basic,identity.email,identity.team",
      "user"=>{ "name"=>"Jasmin Hudacsek",
                "id"=>"U3XMYMF4Z",
                "email"=>"jasmin@hudacsek.com"
              }
    }
  end

  it 'obtains user information from Slack' do
    slack_service = SlackService.new("code")
    allow(slack_service)
    .to receive(:fetch_user_info_from_slack)
    .and_return(slack_user_info)

    expect(slack_service.fetch_user_info_from_slack).to be_instance_of(Hash)
  end
end
