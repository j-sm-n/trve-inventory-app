FactoryGirl.define do
  factory :amount, class: 'Amount' do
    measurement { Faker::Space.distance_measurement }
  end
end
