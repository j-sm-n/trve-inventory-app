FactoryGirl.define do
  factory :packaging_run, class: 'PackagingRun' do
    number { Faker::Number.number(3) }
  end
end
