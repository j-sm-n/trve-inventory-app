FactoryGirl.define do
  factory :product, class: 'Product' do
    name { Faker::Beer.name }
    sku { Faker::Number.number(7) }
    description { Faker::Lorem.paragraph }
  end
end
