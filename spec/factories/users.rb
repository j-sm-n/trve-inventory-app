FactoryGirl.define do
  factory :user, class: 'User' do
    name { Faker::Name.name }
    email { Faker::Internet.email }
    slack_id { Faker::Number.number(10) }
  end
end
