FactoryGirl.define do
  factory :inventory_item, class: 'InventoryItem' do
    product
    packaging_run
    date_packaged '2006-06-24'
  end
end
