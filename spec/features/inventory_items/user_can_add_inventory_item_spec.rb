require 'rails_helper'

RSpec.describe 'Inventory item', type: :feature do
  xscenario 'can be added by a user' do
    user = create :user
    login user
    product = create :product
    amount = create :amount
    visit new_inventory_item_path
    fill_in 'Packaging Run ID', with: '666'
    select "#{product.name}", from: 'Product'
    fill_in 'Date Packaged', with: '06/06/2006'
    select "#{amount.measurement}", from: 'Amount'
    fill_in 'quantity', with: 20
    click_on 'Add Inventory'

    item = InventoryItem.last

    expect(InventoryItem.count).to eq(1)
    expect(item.product.name).to eq(product.name)
    expect(item.date_packaged.to_s).to eq('2006-06-06')
    expect(item.amounts).to eq([amount])
    expect(item.inventory_item_amounts.last.quantity).to eq(20)
    expect(item.packaging_run.number).to eq('666')
    expect(item.packaging_run.on_hold).to be true
    expect(current_path).to eq(inventory_items_path)
  end
end
