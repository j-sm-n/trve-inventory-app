require 'rails_helper'

RSpec.describe 'User logout', type: :feature do
  scenario 'allows sign in user to logout' do
    skip
    user = create :user
    login user
    visit root_path
    click_link 'Logout'

    expect(page).to have_link 'Login'
    expect(page).to_not have_content "Logged in as #{user.name}"
    expect(page).to have_content 'You are now signed out.'
  end
end
