require 'rails_helper'

RSpec.describe 'User login', type: :feature do
  it 'displays Sign in with Slack button' do
    visit login_path

    expect(page).to have_content('Welcome to simple and smarter inventory tracking')

    within('#login-form') do
      expect(page).to have_content('Are you a TRVE Brewing Company team member?')
    end

    expect(page).to have_link('Sign in with Slack')
  end

  it 'allows user to sign in and view current inventory app' do
    user = create(:user)
    login(user)
    visit root_path

    expect(page).to have_content('Current Inventory')
    expect(page).to have_content("Logged in as #{user.name}")
    expect(page).to_not have_link('Login')
  end
end
