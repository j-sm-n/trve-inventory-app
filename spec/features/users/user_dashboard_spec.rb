require 'rails_helper'

RSpec.describe 'After user logs in' do
  it 'they see all navigational links' do
    user = create :user
    login user
    visit root_path

    expect(page).to have_link('View Current Inventory')
    expect(page).to have_link('+ Add New Inventory')
    expect(page).to have_link('- Record Inventory Removal')
    expect(page).to have_link('Profile Settings')
    expect(page).to have_link('Logout')
  end
end
