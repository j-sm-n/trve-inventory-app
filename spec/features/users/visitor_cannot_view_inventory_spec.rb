require 'rails_helper'

RSpec.describe 'Visitor permissions' do
  it 'do not allow visitor from accessing anything but login view' do
    visit inventory_items_path

    expect(current_path).to eq(login_path)
  end
end
