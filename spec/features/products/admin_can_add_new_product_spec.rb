require 'rails_helper'

RSpec.describe 'Product', type: :feature do
  scenario 'can be added by an admin' do
    user = create :user
    login user
    user.add_role :admin
    visit new_product_path
    fill_in 'Name', with: 'Tunnel of Trees'
    fill_in 'SKU', with: '111111111'
    fill_in 'Description', with: 'One hell of a tasty IPA!'
    click_on 'Add Product'

    product = Product.last

    expect(page).to have_content('Product Successfully Added')
    expect(Product.count).to eq(1)
    expect(product.name).to eq('Tunnel of Trees')
    expect(product.sku).to eq('111111111')
    expect(product.description).to eq('One hell of a tasty IPA!')
    expect(current_path).to eq(products_path)
  end

  scenario 'cannot be added by regular user' do
    user = create :user
    login user
    visit new_product_path

    expect(current_path).to eq('/')
  end
end
