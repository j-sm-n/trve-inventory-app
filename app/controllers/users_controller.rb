class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update(user_params)
      flash[:success] = 'Profile Information Successfully Updated'
      redirect_to user_path(@user.id)
    else
      flash[:danger] = @user.errors.full_messages.join(', ')
      redirect_to edit_user_path(@user.id)
    end
  end

  private
  def user_params
    params.require(:user).permit(:name, :email)
  end
end
