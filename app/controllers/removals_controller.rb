class RemovalsController < ApplicationController
  before_action :authorize_user # will replace with CanCanCan
  before_action :confirm_removal_authorized, only: [:show, :undo]
  skip_before_action :reset_undo_status, only: [:show, :undo]
  load_and_authorize_resource

  def new
    @prids =  if PackagingRun.find_by(number: params[:prid])
                params[:prid]
              else
                PackagingRun.where(on_hold: false).order(number: :desc).pluck(:number)
              end
    @reasons = %w{Order Taproom Loss\ &\ Destruction}
  end

  def create
    inventory_item = PackagingRun.find_associated_inv_item(params[:packaging_run])
    if removal = inventory_item.create_new_removal(item_params, current_user.id)
      session[:confirm_removal] = 'true'
      redirect_to removal_path(removal)
    else
      flash[:danger] = inventory_item.errors.full_messages.join(', ')
      redirect_to new_removal_path
    end
  end

  def show
    @removal = Removal.find(params[:id])
    @inventory_item = @removal.inventory_item
    flash[:info] = "Confirm Removal for ID: #{@inventory_item.packaging_run.number}"
  end

  def undo
    flash[:danger] = 'Removal Deleted and Changes Were Not Saved'
    removal = Removal.find(params[:removal_id])
    inventory_item = removal.inventory_item
    inventory_item.undo_removal_creation(removal.id)
    session[:confirm_removal] = nil
    redirect_to new_removal_path
  end

  private
  def item_params
    desired_params = [:reason, :description] + Amount.pluck(:measurement)
    params.permit(desired_params)
  end

  def confirm_removal_authorized
    raise ActionController::RoutingError.new('Not Found') unless session[:confirm_removal]
  end
end
