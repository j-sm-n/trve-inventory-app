class Api::V1::InventoryItemsController < ApplicationController
  def show
    inventory_item = PackagingRun.find_associated_inv_item(params[:id])
    amounts = inventory_item.inventory_item_amounts.reduce([]) do |result, iia|
      result << [iia.amount.measurement, iia.quantity]
    end
    render json: {
      id: inventory_item.id,
      product: inventory_item.product.name,
      amounts: amounts
    }
  end
end
