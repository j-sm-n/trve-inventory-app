class InventoryItemsController < ApplicationController
  before_action :authorize_user # will replace with CanCanCan
  load_and_authorize_resource param_method: :item_params

  def index
    flash.clear
    flash[:success] = 'Removal Successfully Created' if params[:success]
    @inventory_items = InventoryItem.group_by_index_display
    @amounts = Amount.pluck(:id, :measurement)
  end

  def new
    @inventory_item = InventoryItem.new
  end

  def create
    @inventory_item = InventoryItem.new(product_id: item_params[:product_id],
                                        packaging_run: PackagingRun.create(number: params["Packaging Run ID"]),
                                        date_packaged: item_params[:date_packaged])
    if @inventory_item.save_new_packaging_run(amounts: params[:amount], quantities: params[:quantity])
      redirect_to inventory_items_path
      flash[:success] = 'Item Successfully Added to Inventory'
    else
      flash.now[:danger] = @inventory_item.errors.full_messages.join(', ')
      render :new
    end
  end

  def destroy
    inventory_item = InventoryItem.find(params[:id])
    inventory_item.delete_all
    flash[:success] = 'Item Permanently Deleted from Database'
    redirect_to inventory_items_path
  end

  def edit
    @inventory_item = InventoryItem.find(params[:id])
  end

  def update
    @inventory_item = InventoryItem.find(params[:id])
    if @inventory_item.update_all(params)
      redirect_to inventory_items_path
      flash[:success] = 'Item Successfully Updated'
    else
      flash.now[:danger] = @inventory_item.errors.full_messages.join(', ')
      render :edit
    end
  end

  private

  def item_params
    params.require(:inventory_item).permit(:product_id, :date_packaged, :amounts)
  end
end

@inventory_items = InventoryItem.joins(:packaging_run)
                                .merge(PackagingRun.order(on_hold: :asc))
                                .order(id: :desc)
