class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :reset_undo_status

  # To handle custom view for unauthorized access, modify the code below:
  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.json { head :forbidden, content_type: 'text/html' }
      format.html { redirect_to main_app.root_url, notice: exception.message }
      format.js   { head :forbidden, content_type: 'text/html' }
    end
  end

  helper_method :current_user, :logged_in?

  def current_user
    User.find(session[:user_id]) if session[:user_id]
  end

  def logged_in?
    current_user != nil
  end

  def authorize_user
    unless logged_in?
      redirect_to login_path
    end
  end

  def reset_undo_status
    session[:confirm_removal] = nil
  end
end
