class ProductsController < ApplicationController
  before_action :authorize_user
  load_and_authorize_resource param_method: :product_params

  def index
    @products = Product.order(created_at: :desc)
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      redirect_to products_path
      flash[:success] = 'Product Successfully Added'
    else
      flash.now[:danger] = @product.errors.full_messages.join(', ')
      render :new
    end
  end

  private

  def product_params
    params.require(:product).permit(:name, :sku, :description)
  end
end
