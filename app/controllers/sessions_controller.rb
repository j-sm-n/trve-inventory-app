class SessionsController < ApplicationController  
  def new
  end

  def create
    if params["code"]
      slack_user = get_slack_user_attributes(params["code"])
      if user = User.from_omniauth(slack_user)
        session[:user_id] = user.id
      end
      redirect_to inventory_items_path
    else
      redirect_to root_path
    end
  end

  def get_slack_user_attributes(code)
    slack_user_access = SlackService.new(code)
    user_info = slack_user_access.fetch_user_info_from_slack
    SlackUser.new(user_info)
  end

  def destroy
    session.clear
    flash[:notice] = 'You are now signed out.'
    redirect_to login_path
  end
end
