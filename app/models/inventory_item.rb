class InventoryItem < ApplicationRecord
  resourcify
  validates :date_packaged, presence: true
  validates :packaging_run_id, presence: true

  belongs_to :product
  belongs_to :packaging_run
  has_many :inventory_item_amounts, dependent: :destroy
  has_many :amounts, through: :inventory_item_amounts
  has_many :removals, dependent: :destroy

  def create_new_removal(params, user_id)
    removal = self.removals.new(user_id: user_id,
                                reason: params[:reason],
                                description: params[:description])
    measurements = set_measurements(params)
    if removal.valid? && valid_removal_amounts?(measurements)
      removal.save
      measurements.each do |measurement, quantity|
        if quantity > 0
          removal.removal_amounts.create( amount: Amount.find_by(measurement: measurement),
                                          quantity: quantity)
          update_iia(measurement, quantity)
        end
      end
      removal
    else
      false
    end
  end

  def undo_removal_creation(removal_id)
    removal = Removal.find(removal_id)
    removal.removal_amounts.each do |ra|
      iia = self.inventory_item_amounts.find_by(amount_id: ra.amount_id)
      iia.quantity += ra.quantity
      iia.save!
      ra.delete
    end
    removal.delete
  end

  def save_new_packaging_run(**params)
    params[:quantities] ||= ['']
    empty_quantities = params[:quantities].any? {|a| a.empty?}
    if !empty_quantities && self.save
      params[:amounts].each_with_index do |amount, i|
        inventory_item_amount = self.inventory_item_amounts.find_or_create_by(amount_id: amount)
        inventory_item_amount.quantity = inventory_item_amount.quantity.to_i + params[:quantities][i].to_i
        inventory_item_amount.save
      end
    else
      self.valid?
      self.errors.add(:base, "Quantity can't be blank") if empty_quantities
      false
    end
  end

  def update_all(params)
    params[:quantity] ||= ['']
    empty_quantities = params[:quantity].any? {|a| a.empty?}
    self.product_id = params[:inventory_item][:product_id]
    self.packaging_run.number = params['Packaging Run ID']
    self.date_packaged = params[:inventory_item][:date_packaged]
    if self.packaging_run.valid? && self.valid? && !empty_quantities
      clear_quantities(params[:amount])
      params[:amount].each_with_index do |amount, i|
        inventory_item_amount = self.inventory_item_amounts.find_or_create_by(amount_id: amount)
        inventory_item_amount.quantity = inventory_item_amount.quantity.to_i + params[:quantity][i].to_i
        inventory_item_amount.save
      end
      self.packaging_run.save
      self.save
    else
      self.errors.add(:base, "Quantity can't be blank") if empty_quantities
      self.errors.add(:base, "Packaging Run ID already taken") unless self.packaging_run.valid?
      false
    end
  end

  def quantity_for(amount_id)
    query = generate_query :measurement_amounts, inventory_item_id: self.id
    @measurement_amounts ||=  InventoryItem.find_by_sql(query)
    @measurement_amounts = convert_to_hash unless @measurement_amounts.is_a? Hash
    @measurement_amounts[amount_id.to_i].to_i
  end

  def on_hold?
    packaging_run.on_hold
  end

  def delete_all
    self.packaging_run.delete
    self.delete
  end

  def available_amounts_and_removals(removal_id)
    query = generate_query :quantity_removal, removal_id: removal_id, inventory_item_id: self.id
    InventoryItem.find_by_sql(query)
  end

  def self.group_by_index_display
    query = self.new.generate_query :index_display
    self.find_by_sql(query)
  end

  private

  def clear_quantities(amounts)
    amounts.each do |amount|
      if iia = self.inventory_item_amounts.find_by(amount_id: amount)
        iia.quantity = 0
        iia.save
      end
    end
  end

  def set_measurements(params)
    measurements = Amount.pluck(:measurement)
    params.keys.reduce({}) do |result, param|
      result[param] = params[param].to_i if measurements.include?(param)
      result
    end
  end

  def valid_removal_amounts?(measurements)
    if measurements.values.all? {|a| a.zero?}
      self.errors.add(:base, "No amounts marked to be removed")
      return false
    end
    measurements.each do |measurement, quantity|
      quantity_limits = self.inventory_item_amounts
                            .includes(:amount)
                            .find_by(amounts: {measurement: measurement})
                            .quantity
      if quantity > quantity_limits
        self.errors.add(:base, "Quantity can't be larger than amount in packaging run")
        return false
      elsif quantity < 0
        self.errors.add(:base, "Quantity can't be less than zero")
        return false
      end
    end
    true
  end

  def update_iia(measurement, quantity)
    iia = self.inventory_item_amounts
              .includes(:amount)
              .find_by(amounts: {measurement: measurement})
    difference = iia.quantity - quantity
    iia.update(quantity: difference)
  end

  def convert_to_hash
    @measurement_amounts.reduce({}) do |result, data|
      result[data.amount_id] = data.quantity
      result
    end
  end
end
