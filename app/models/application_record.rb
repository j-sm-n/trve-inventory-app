class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def generate_query(query, **params)
    self.send(query, params)
  end

  private
  def quantity_removal(**params)
    "SELECT iia.measurement,
    COALESCE(
      (SELECT removal_amounts.quantity
      FROM removals
      INNER JOIN removal_amounts ON removals.id = removal_amounts.removal_id
      WHERE removals.inventory_item_id = iia.inventory_item_id
      AND removal_amounts.amount_id = iia.amount_id
      AND removals.id = #{params[:removal_id]}), 0) AS removal,
    iia.quantity
    FROM
      (SELECT inventory_item_amounts.id AS id, inventory_item_id, amount_id, quantity, amounts.measurement AS measurement
      FROM inventory_item_amounts
      INNER JOIN amounts ON inventory_item_amounts.amount_id = amounts.id
      WHERE inventory_item_id = #{params[:inventory_item_id]}) iia;"
  end

  def index_display(**params)
    "SELECT SUM(iia.quantity) AS quantity,
    pr.on_hold AS hold,
    inventory_items.*
    FROM inventory_items
    INNER JOIN inventory_item_amounts AS iia ON iia.inventory_item_id = inventory_items.id
    INNER JOIN packaging_runs AS pr ON inventory_items.packaging_run_id = pr.id
    WHERE quantity > 0
    GROUP BY inventory_items.id, pr.on_hold
    ORDER BY pr.on_hold, inventory_items.date_packaged DESC;"
  end

  def measurement_amounts(**params)
    "SELECT iia.amount_id, iia.quantity
    FROM
      (SELECT id
      FROM inventory_items
      WHERE id = #{params[:inventory_item_id]}) ii
    INNER JOIN inventory_item_amounts AS iia ON ii.id = iia.inventory_item_id
    GROUP BY iia.id, iia.amount_id;"
  end
end
