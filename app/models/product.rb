class Product < ApplicationRecord
  resourcify
  validates :name, presence: true, uniqueness: true
  validates :sku, presence: true, uniqueness: true
  validates :description, presence: true

  has_many :inventory_items
end
