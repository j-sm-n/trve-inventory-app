class InventoryItemAmount < ApplicationRecord
  resourcify
  validates :quantity, presence: true
  validates_numericality_of :quantity

  belongs_to :inventory_item
  belongs_to :amount
end
