class Amount < ApplicationRecord
  resourcify
  validates :measurement, presence: true

  has_many :inventory_item_amounts
  has_many :inventory_items, through: :inventory_item_amounts
  has_many :removal_amounts
end
