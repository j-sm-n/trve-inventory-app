class SlackUser
  attr_reader :access_token,
              :name,
              :slack_id,
              :email

  def initialize(slack_user_info)
    @access_token  = slack_user_info["access_token"]
    @name          = slack_user_info["user"]["name"]
    @slack_id      = slack_user_info["user"]["id"]
    @email         = slack_user_info["user"]["email"]
  end
end
