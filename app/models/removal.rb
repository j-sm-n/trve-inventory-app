class Removal < ApplicationRecord
  resourcify
  validates :reason, presence: true

  belongs_to :user
  belongs_to :inventory_item
  has_many :removal_amounts, dependent: :destroy
  has_many :amounts, through: :removal_amounts

  def delete_all
    self.removal_amounts.each { |ra| ra.delete }
    self.delete
  end
end
