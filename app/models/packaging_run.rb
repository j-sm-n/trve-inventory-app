class PackagingRun < ApplicationRecord
  resourcify
  validates :number, presence: true, uniqueness: true

  # validates :on_hold, presence: true

  has_many :inventory_items

  def self.find_associated_inv_item(number)
    InventoryItem.includes(:packaging_run)
                  .find_by(packaging_runs: {number: number})
  end
end
