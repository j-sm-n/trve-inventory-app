class RemovalAmount < ApplicationRecord
  resourcify
  validates :quantity, presence: true
  validates_numericality_of :quantity

  belongs_to :removal
  belongs_to :amount
end
