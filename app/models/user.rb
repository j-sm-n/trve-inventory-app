class User < ApplicationRecord
  rolify
  validates :name, presence: true
  validates :email, presence: true
  after_create :assign_default_role

  def assign_default_role
    self.add_role(:staff) if self.roles.blank?
  end

  def self.from_omniauth(auth_info)
    where(slack_id: auth_info.slack_id).first_or_create do |new_user|
      new_user.access_token = auth_info.access_token
      new_user.slack_id     = auth_info.slack_id
      new_user.name         = auth_info.name
      new_user.email        = auth_info.email
    end
  end
end
