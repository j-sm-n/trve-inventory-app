$(document).on("turbolinks:load", function () {
  var $pridRemoval = $('#prid-removal')
  var $reasonRemoval = $('#reason-removal')

  $pridRemoval.on('change', function () {
    var prid = $('select option:selected')[0].text
    $.ajax({
      dataType: 'json',
      url: '/api/v1/inventory_items/' + prid + '.json',
      success: function (data) {
        $('#amounts-removal').empty()
        var measurements = ''
        for (var i = 0; i < data.amounts.length; i++) {
          measurements += amountSelectHtml(data.amounts[i])
        }
        $('#amounts-removal').append(
          "<table class='table table-sm table-striped'>" +
            "<tr><th>Measurement</th><th>Total Quantity</th><th>Amount to Remove</th></tr>" +
            measurements +
          "</table>"
        )
      }
    })
  })

  $reasonRemoval.on('change', function (event) {
    var last = $('select option:selected').length - 1
    var reason = $('select option:selected')[last].text
    if (reason == 'Loss & Destruction') {
      $('#reason-description').show(200)
    } else {
      $('#reason-description').hide(200)
    }
  })

  if ($('div.removals').length > 0) {
    var prid
    if ($('select option:selected').length > 1) {
      prid = $('select option:selected')[0].text
    } else {
      prid = $('#prid-removal').val()
    }
    $.ajax({
      dataType: 'json',
      url: '/api/v1/inventory_items/' + prid + '.json',
      success: function (data) {
        $('#amounts-removal').empty()
        var measurements = ''
        for (var i = 0; i < data.amounts.length; i++) {
          measurements += amountSelectHtml(data.amounts[i])
        }
        $('#amounts-removal').append(
          "<table class='table table-sm table-striped'>" +
          "<tr><th>Measurement</th><th>Total Quantity</th><th>Amount to Remove</th></tr>" +
          measurements +
          "</table>"
        )
      }
    })
  }
})

function amountSelectHtml(amount) {
  result =  "<tr>" +
              "<td>" + amount[0] + "</td>" +
              "<td>" + amount[1] + "</td>" +
              "<td><input type='number' name='" + amount[0] + "' id='quantity_' value='0' min='0' max=" + amount[1] + "></td>" +
            "</tr>"
  return result
}
