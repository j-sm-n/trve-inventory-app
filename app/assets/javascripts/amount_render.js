$(document).ready(function () {
  this.addAmount = function (event) {
    event.preventDefault()
    $('#amounts').append($('#new_amount_form').html())
  }

  this.removeAmount = function (event, element) {
    event.preventDefault()
    element.parent().remove()
  }
})
