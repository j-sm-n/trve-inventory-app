module ApplicationHelper
  def format_product_date(product)
    product.created_at.strftime('%m/%d/%y %I:%M:%S %p')
  end
end
