Rails.application.routes.draw do
  namespace :api  do
    namespace :v1 do
      resources :inventory_items, only: [:show]
    end
  end
  get '/sessions/login' => 'sessions#new', as: 'login'
  delete '/sessions/logout' => 'sessions#destroy', as: 'logout'
  get '/auth/:provider/create_session' => 'sessions#create'

  root to: 'inventory_items#index'

  resources :inventory_items, only: [:index, :new, :create, :edit, :update, :destroy]

  resources :removals, only: [:new, :create, :show] do
    post '/undo' => 'removals#undo', as: 'undo'
  end

  resources :products, only: [:create, :new, :index]

  resources :users, only: [:show, :edit, :update]
end
